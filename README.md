A django powered app to add and manage books to a library.

Database Design
===============

Books App
---------

	Author
		- name
		- email
		- phone

	Publisher
		- name
		- website
		- email
		- city

	Book
		- title
		- authors
		- publisher
		- added_user
		- publication_date

Users App
---------

	User
		- name
		- username
		- sha1_password
		- registration_date