from django.shortcuts import render
from books.forms import *
from users.models import User
from books.models import *
from hashlib import sha1
from django.http import HttpResponseRedirect

# Create your views here.
def addAuthor(request):
	d = {'title': 'Library :: Add Author'}
	if 'user' in request.session:
		d['name'] = request.session['user'].name
	else:
		return HttpResponseRedirect('/login/')
	form = AddAuthorForm(request.POST or None)
	d['form'] = form
	if form.is_valid():
		data = form.cleaned_data
		author = Author(name=data['name'], email=data['email'], phone=data['phone'])
		author.save()
		return HttpResponseRedirect('/dashboard/')
	return render(request, 'add_author.html', d)

def addPublisher(request):
	d = {'title':'Library :: Add Publisher'}
	if 'user' in request.session:
		d['name'] = request.session['user'].name
	else:
		return HttpResponseRedirect('/login/')
	
	form = AddPublisherForm(request.POST or None)
	if form.is_valid():
		data = form.cleaned_data
		publisher = Publisher(name=data['name'], website=data['website'], 
			email=data['email'], city=data['city'])
		publisher.save()
		return HttpResponseRedirect('/dashboard/')
	d['form'] = form
	return render(request, 'add_publisher.html', d)

def addBook(request):
	d = {'title':'Library :: Add Book'}
	if 'user' in request.session:
		d['name'] = request.session['user'].name
	else:
		return HttpResponseRedirect('/login/')
	form = AddBookForm(request.POST or None)
	if form.is_valid():
		data = form.cleaned_data
		book = Book(title=data['title'],  
			publisher=data['publisher'], publication_date=data['publication_date'], 
			added_user=User.objects.get(username=request.session['user'].username))
		book.save()
		book.authors.add(*data['authors'])
		book.save()
		return HttpResponseRedirect('/dashboard/')
	d['form'] = form
	return render(request, 'add_book.html', d)