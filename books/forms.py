import datetime
from django import forms
from django.core.validators import RegexValidator, URLValidator, EmailValidator, validate_email
from books.models import *
from django.contrib.admin import widgets

class AddAuthorForm(forms.Form):
	name = forms.CharField(max_length=30)
	email = forms.EmailField()
	phone = forms.IntegerField(validators=[RegexValidator(regex='^\d{10}$', message='Length has to be 10', code='Invalid number')])

class AddPublisherForm(forms.Form):
	name = forms.CharField(max_length=30)
	website = forms.CharField(max_length=200, validators=[URLValidator()])
	email = forms.CharField(max_length=75, validators=[validate_email])
	city = forms.CharField(max_length=25, validators=[RegexValidator(regex='^[a-zA-Z\h]+$', message='Should be characters(space allowed)', code='Invalid city name')])

class AddBookForm(forms.Form):
	title = forms.CharField(max_length=50)
	authors = forms.ModelMultipleChoiceField(queryset=Author.objects.all())
	publisher = forms.ModelChoiceField(queryset=Publisher.objects.all(), initial=Publisher.objects.get() if Publisher.objects.count() > 0 else None)
	publication_date = forms.DateField()
	# def __init__(self, *args, **kwargs):
	# 	super(AddBookForm, self).__init__(*args, **kwargs)
	# 	self.fields['publication_date'].widget = widgets.AdminDateWidget()