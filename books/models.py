from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from users.models import User

# Create your models here.
class Publisher(models.Model):
	name = models.CharField(max_length=30, unique=True)
	website = models.CharField(max_length=200)
	email = models.EmailField(max_length=75, unique=True)
	city = models.CharField(max_length=25)

	def __unicode__(self):
		return self.name

class Author(models.Model):
	name = models.CharField(max_length=30, unique=True)
	email = models.EmailField(max_length=50)
	phone = models.CharField(max_length=10, unique=True, validators=[RegexValidator(regex='^\d{10}$', message='Length has to be 10', code='Invalid number')])

	def __unicode__(self):
		return self.name

class Book(models.Model):
	title = models.CharField(max_length=50, unique=True)
	authors = models.ManyToManyField(Author)
	publisher = models.ForeignKey(Publisher)
	added_user = models.ForeignKey(User)
	publication_date = models.DateField()

	def __unicode__(self):
		return self.title