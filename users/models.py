from django.db import models
from django.core.validators import RegexValidator

# Create your models here.
class User(models.Model):
	name = models.CharField(max_length=30)
	username = models.CharField(max_length=20, unique=True, 
		validators=[RegexValidator(regex='^\w{6.20}$', 
			message='Username should contain only alphanumerics and underscores')])
	registration_date = models.DateField(auto_now_add=True)

	def __unicode__(self):
		return self.name

class PassGens(models.Model):
	user = models.ForeignKey(User)
	sha1_pass = models.CharField(max_length=50)
	def __unicode__(self):
		return self.sha1_pass