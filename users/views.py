from django.shortcuts import render
from users.forms import *
from users.models import User, PassGens
from books.models import *
from hashlib import sha1
from django.http import HttpResponseRedirect

def login(request):
	d = {'login_title': 'Library :: Login'}
	if request.method == 'POST':
		form = LoginForm(request.POST)
		status = 'Login Failed'
		if form.is_valid():
			data = form.cleaned_data
			try:
				user = User.objects.get(username=data['username'])
				sha1_p = PassGens.objects.get(user=user)
				if sha1_p.sha1_pass == sha1(data['password']).hexdigest():
					request.session['user'] = user
					return HttpResponseRedirect('/dashboard/')
			except (ValueError, User.DoesNotExist):
				status = 'Login Failed'
		d['status'] = status
	else:
		form = LoginForm()
	d['form'] = form
	return render(request, 'login.html', d)

def register(request):
	if request.method == 'POST':
		form = RegisterForm(request.POST)

		if form.is_valid():
			data = form.cleaned_data
			user = User(name=data['name'], username=data['username'])
			user.save()
			passw = PassGens(user=user, sha1_pass=sha1(data['password']).hexdigest())
			passw.save()
			
			return HttpResponseRedirect('/')
	else:
		form = RegisterForm()

	return render(request, 'register.html', {'register_title': 'Library :: Register', 'form': form})

def dashboard(request):
	d = {'dashboard_title':'Library :: Dashboard'}
	if 'user' in request.session:
		d['name'] = request.session['user'].name
	else:
		return HttpResponseRedirect('/login/')
	return render(request, 'dashboard.html', d)

