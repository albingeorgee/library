from django import forms
from django.core.validators import RegexValidator

class LoginForm(forms.Form):
    username = forms.CharField(max_length=20, required=True)
    password = forms.CharField(max_length=30, widget=forms.PasswordInput, required=True)

class RegisterForm(forms.Form):
	name = forms.CharField(max_length=25)
	username = forms.CharField(max_length=20, 
		validators=[RegexValidator(regex='^\w{6,20}$', 
			message='Username should contain only alphanumerics and underscores')])
	password = forms.CharField(max_length=30, widget=forms.PasswordInput)

class AddBookForm(forms.Form):
	title = forms.CharField(max_length=50)
	authors = forms.CharField(max_length=30, widget=forms.Select)

	# def __init__(self, author_names, *args, **kwargs):
	# 	super(AddBookForm, self).__init__(*args, **kwargs)
	# 	self

class AddAuthorForm(forms.Form):
	name = forms.CharField(max_length=30)
	email = forms.EmailField()
	phone = forms.IntegerField(validators=[RegexValidator(regex='^\d{10}$', message='Length has to be 10', code='Invalid number')])