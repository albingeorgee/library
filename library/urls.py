from django.conf.urls import patterns, include, url
import library.views
import users.views
from django.views.static import * 
from django.conf import settings

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^$', 'library.views.home'),
    url(r'^login/$', 'users.views.login'),
    url(r'^logout/$', 'library.views.logout'),
    url(r'^register/$', 'users.views.register'),
    url(r'^dashboard/$', 'users.views.dashboard'),
    url(r'^addBook/$', 'books.views.addBook'),
    url(r'^addAuthor/$', 'books.views.addAuthor'),
    url(r'^addPublisher/$', 'books.views.addPublisher'),
    
    # url(r'^library/', include('library.foo.urls')),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
