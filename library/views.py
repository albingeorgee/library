from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.template import Template, Context

def home(request):
	d = {'home_title': 'Library :: Home'}
	if 'user' in request.session:
		d['name'] = request.session['user'].name
		return HttpResponseRedirect('/dashboard/')
	
	return render(request, 'home.html', d)

def logout(request):
	del request.session['user']
	return HttpResponseRedirect('/')